﻿using System.Collections.Generic;
using System.Threading.Tasks;
using datascience_sp_api.Dtos;
using datascience_sp_api.Repositories;
using Microsoft.AspNetCore.Mvc;
using StatsdClient;

namespace datascience_sp_api.Controllers
{
    //[Authorize(Policy = "ReadAddressBook")]
    public class DataScienceController : ControllerBase
    {
       private readonly IDataScienceRepository _dataScienceRepository;

        public DataScienceController(IDataScienceRepository dataScienceRepository)
        {
            _dataScienceRepository = dataScienceRepository;
        }

        [HttpPost("storedprocedurecall")]
        public async Task<ActionResult<IEnumerable<dynamic>>> StoredProcedureCall([FromBody] StoredProcedureRequestDto request)
        {
            using (DogStatsd.StartTimer($"{nameof(DataScienceController)}.StoredProcedureCall")) {
                var retVal = await _dataScienceRepository.CallStoredProcedure(request);
                if (retVal != null) {
                    return Ok(retVal);
                }

                return BadRequest();
            };
        }
    }
}
