﻿The purpose of this project is to create an api endpoint that can accept a dynamic object used to call a stored procedure and return a dynamic result.

The project is designed to run in a docker container within a cloud envrionment. 

The database connection can be configured with environment variables set within it's stack.

The request can contain any accessable stored procedure (with parameters) that is exposed with read/execute permissions.

The reponse will be a dynamically generated JSON array.

Example Request:

{
  "schema": "dbo",
  "name": "sp_promotionImpactCalculator",
  "parameters": {
    "@PromotionType": "shipping",
    "@startDate": "2020-05-24",
    "@EndDate": "2020-05-25",
    "@Commission_or_Product" : "product",
    "@OverLappingPromos" : 1,
    "@OverLappingLaunchs" : 0
  }
}

Example Response:

[
  {
    "StartDate": "2020-05-24T00:00:00",
    "EndDate": "2020-05-25T00:00:00",
    "PromotionType": "shipping",
    "season": "spring",
    "Month_Name": "May",
    "IntroPeriod": "EOM",
    "PromotionType_blackfriday": false,
    "PromotionType_bogo": false,
    "PromotionType_event": false,
    "PromotionType_free": false,
    "PromotionType_holiday": false,
    "PromotionType_monthLong": false,
    "PromotionType_other": false,
    "PromotionType_product": false,
    "PromotionType_shipping": true,
    "CommissionBasedYN": false,
    "ProductBasedYN": true,
    "season_fall": false,
    "season_spring": true,
    "season_summer": false,
    "season_winter": false,
    "doy_start": 145,
    "doy_end": 146,
    "Days_Cnt": 1,
    "DaysSinceLastPromo": 0,
    "previousAutoShip": 9,
    "nextAutoShip": 11,
    "IntroPeriod_BOM": false,
    "IntroPeriod_EOM": true,
    "IntroPeriod_MOM": false,
    "OverLappingPromos": 1,
    "OverLappingLaunchs": 0,
    "AvgRev1WK_beforePromo": 398.86,
    "HolidayCnt": 1,
    "WeekendCnt": 1,
    "predict": 72.3051
  }
]