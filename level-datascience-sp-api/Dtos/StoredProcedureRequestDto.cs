﻿using System.Collections.Generic;

namespace datascience_sp_api.Dtos
{
    public class StoredProcedureRequestDto
    {
        public string Schema { get; set; }
        public string Name { get; set; }
        
        public IDictionary<string, dynamic> Parameters { get; set; }
    }
}
