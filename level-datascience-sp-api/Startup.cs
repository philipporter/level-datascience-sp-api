﻿using datascience_sp_api.Repositories;
using LeVel.Microservices.Core.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace datascience_sp_api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly ILoggerFactory _loggerFactory;
        
        public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
        {
            _configuration = configuration;
            _loggerFactory = loggerFactory;
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddLeVelSetup(_configuration, _loggerFactory)
                .AddAuthorization(options =>
                {
                    options.AddPolicy("ReadAddressBook",
                        policy =>
                        policy.RequireClaim("scope", "user-profile.addressbook.read"));

                })
                .AddTransient<IDataScienceRepository, DataScienceRepository>()
                .AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseLeVelSetup(_configuration);
        }
    }
}
