﻿using Dapper;
using datascience_sp_api.Dtos;
using Microsoft.Extensions.Configuration;
using OpenTracing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace datascience_sp_api.Repositories
{
    public class DataScienceRepository : IDataScienceRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ITracer _tracer;

        public DataScienceRepository(IConfiguration configuration, ITracer tracer)
        {
            _configuration = configuration;
            _tracer = tracer;
        }

        public async Task<IEnumerable<dynamic>> CallStoredProcedure(StoredProcedureRequestDto request)
        {
            try {
                using (var scope = _tracer.BuildSpan("DataScience.CallStoredProcedure").StartActive(true)) {
                    
                    scope.Span.SetTag("sp_schema", request.Schema);
                    scope.Span.SetTag("sp_name", request.Name);
                    
                    var connection  = new SqlConnection(_configuration.GetConnectionString("DBRead"));

                    var parameters = new DynamicParameters();
                    
                    foreach (var param in request.Parameters) 
                    { 
                        scope.Span.SetTag($"sp_param_{param.Key}_key", param.Key);
                        scope.Span.SetTag($"sp_param_{param.Key}_value", param.Value);
                        
                        parameters.Add(param.Key, param.Value); 
                    }

                    parameters.Add("@return_value", dbType: DbType.String, direction: ParameterDirection.ReturnValue);

                    IEnumerable<dynamic> query = await connection.QueryAsync($"{request.Schema}.{request.Name}", parameters, commandType: CommandType.StoredProcedure);

                    int result = parameters.Get<int>("@return_value");
                    
                    scope.Span.SetTag("sp_result", result);

                    if (result == 0)
                    {
                        return query;
                    }

                    return null;
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
                throw;
            }
        }
    }
}
