﻿using System.Collections.Generic;
using System.Threading.Tasks;
using datascience_sp_api.Dtos;

namespace datascience_sp_api.Repositories
{
    public interface IDataScienceRepository
    {
        Task<IEnumerable<dynamic>> CallStoredProcedure(StoredProcedureRequestDto request);
    }
}
