﻿using System;
using System.IO;
using LeVel.Containers.AWS;
using LeVel.Microservices.Core.Web;
using LeVel.Microservices.SecretsManagement.AWS;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using LeVel.Microservices.Core.Logging;
using System.Diagnostics;
using Serilog;
using LeVel.OpenTracing.Datadog;

namespace datascience_sp_api
{
    public class Program
    {
        public static int Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(Path.Combine("config", "appsettings.json"), optional: true, reloadOnChange: true)
                //.AddJsonFile(Path.Combine("config", $"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}.json"), optional: true)
                .AddJsonFile(Path.Combine("config", "appsettings.Local.json"), optional: true, reloadOnChange: true)         
                .AddAppSettingsFromAWS()
                .AddEnvironmentVariables()
                .Build();

            var applicationName = configuration["ApplicationName"];
            var environmentName = configuration["EnvironmentName"];

            var logger = LeVelLoggerBuilder.CreateLogger(configuration);

            if (Debugger.IsAttached)
            {
                logger = new LoggerConfiguration()
                    .ReadFrom.Configuration(configuration)
                    .Enrich.FromLogContext()
                    .Enrich.WithProperty("Application", applicationName)
                    .Enrich.WithProperty("Environment", Environment.GetEnvironmentVariable("environment"))
                    .WriteTo.Console()
                    .CreateLogger();
            }

            var hostIp = ContainerHostMetadataExtensions.GetContainerHostIp(configuration);

            var openTracingConfig = new OpenTracingConfiguration();
            configuration.GetSection("OpenTracing").Bind(openTracingConfig);

            openTracingConfig.ApplicationName = (string.IsNullOrWhiteSpace(environmentName)) ? applicationName : $"{environmentName}-{applicationName}"; ;
            openTracingConfig.Host = hostIp;

            try
            {
                logger.Information("Starting web host");
                logger.Information("Tracing to {@host}:{@port}", hostIp, openTracingConfig.Port);

                WebHost
                    .CreateDefaultBuilder(args)
                    .UseLevelSetup(configuration)
                    .ConfigureServices(services =>
                    {
                        services.AddOpenTracing(openTracingConfig);
                    })
                    .UseStartup<Startup>()
                    .Build()
                    .Run();
                return 0;
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
        }
    }
}
